package com.epam.hibernate.khranovskyi.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class CategoryLocalizationDto {
    private UUID id = UUID.randomUUID();

    private String translation;
}

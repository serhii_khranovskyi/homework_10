package com.epam.hibernate.khranovskyi.dto;

import com.epam.hibernate.khranovskyi.entity.Activity;
import com.epam.hibernate.khranovskyi.entity.CategoryLocalization;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class CategoryDto {
    private UUID id;

    private String categoryName;

    private Set<ActivityDto> activities = new HashSet<>();

    private Set<CategoryLocalizationDto> categoryLocalizations = new HashSet<>();
}

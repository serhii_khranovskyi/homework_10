package com.epam.hibernate.khranovskyi.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "activities_localizations")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ActivityLocalization {

    @Id
    @Type(type = "uuid-char")
    private UUID id = UUID.randomUUID();

    private String translation;

    @ManyToOne
    @JoinColumn(name = "activity_id")
    private Category parentActivityLocale;

    @ManyToOne
    @JoinColumn(name = "language_id")
    private Category parentActivityLanguage;
}

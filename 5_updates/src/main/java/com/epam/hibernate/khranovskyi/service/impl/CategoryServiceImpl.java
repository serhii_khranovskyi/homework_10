package com.epam.hibernate.khranovskyi.service.impl;

import com.epam.hibernate.khranovskyi.dao.CategoryDAO;
import com.epam.hibernate.khranovskyi.dto.CategoryDto;
import com.epam.hibernate.khranovskyi.entity.Activity;
import com.epam.hibernate.khranovskyi.entity.Category;
import com.epam.hibernate.khranovskyi.entity.CategoryLocalization;
import com.epam.hibernate.khranovskyi.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private CategoryAssemblerImpl assembler;

    @Override
    public CategoryDto create(CategoryDto dto) {
        Category category = assembler.assemble(dto);
        categoryDAO.save(category);
        return assembler.assemble(category);

    }

    @Override
    @Transactional(readOnly = true)
    public CategoryDto get(String name) {
        Category category = categoryDAO.get(name);
        return assembler.assemble(category);
    }

    @Override
    @Transactional
    public CategoryDto update(CategoryDto dto) {
        Category entity = categoryDAO.get(dto.getId());
        Category updatedEntity = assembler.assemble(dto);

        performUpdate(entity, updatedEntity);

        return assembler.assemble(entity);
    }

    private void performUpdate(Category persistentEntity, Category newEntity) {
        persistentEntity.setId(newEntity.getId());
        persistentEntity.setCategoryName(newEntity.getCategoryName());
        update(persistentEntity.getActivities(), newEntity.getActivities());
        updateEntity(persistentEntity.getCategoryLocalizations(), newEntity.getCategoryLocalizations());
    }

    private void update(Set<Activity> persistentActivity, Set<Activity> newActivity) {
        Map<UUID, Activity> stillExistentActivity = newActivity
                .stream()
                .filter(j -> Objects.nonNull(j.getId()))
                .collect(Collectors.toMap(Activity::getId, Function.identity()));

        Set<Activity> activities = newActivity
                .stream()
                .filter(j -> Objects.isNull(j.getId()))
                .collect(Collectors.toSet());

        Iterator<Activity> persistentIterator = persistentActivity.iterator();
        while (persistentIterator.hasNext()) {
            Activity activity = persistentIterator.next();
            if (stillExistentActivity
                    .containsKey(activity.getId())) {
                Activity updatedActivity = stillExistentActivity.get(activity.getId());
                activity.setParentCategory(updatedActivity.getParentCategory());
            } else {
                persistentIterator.remove();
                activity.setParentCategory(null);
            }
        }
        persistentActivity.addAll(activities);
    }

    private void updateEntity(Set<CategoryLocalization> persistentLocalization, Set<CategoryLocalization> newLocalization) {
        Map<UUID, CategoryLocalization> stillExistentActivity = newLocalization
                .stream()
                .filter(j -> Objects.nonNull(j.getId()))
                .collect(Collectors.toMap(CategoryLocalization::getId, Function.identity()));

        Set<CategoryLocalization> activities = newLocalization
                .stream()
                .filter(j -> Objects.isNull(j.getId()))
                .collect(Collectors.toSet());

        Iterator<CategoryLocalization> persistentIterator = persistentLocalization.iterator();
        while (persistentIterator.hasNext()) {
            CategoryLocalization categoryLocalization = persistentIterator.next();
            if (stillExistentActivity
                    .containsKey(categoryLocalization.getId())) {
                CategoryLocalization localization = stillExistentActivity.get(categoryLocalization.getId());
                categoryLocalization.setParentCategoryLocale(localization.getParentCategoryLocale());
            } else {
                persistentIterator.remove();
                categoryLocalization.setParentCategoryLocale(null);
            }
        }
        persistentLocalization.addAll(activities);
    }

    @Override
    public void delete(UUID id) {
        categoryDAO.delete(id);
    }
}

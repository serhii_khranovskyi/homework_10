package com.epam.hibernate.khranovskyi.dao;

import com.epam.hibernate.khranovskyi.entity.ActivityLocalization;

import java.util.UUID;

public interface ActivityLocalizationDAO {
    void save(ActivityLocalization activityLocalization);

    ActivityLocalization get(UUID activityId);

    ActivityLocalization get(String name);

    void delete(UUID activityLocalizationId);
}
package com.epam.hibernate.khranovskyi.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "roles")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Role {

    @Id
    @Type(type = "uuid-char")
    private UUID id = UUID.randomUUID();

    @OneToMany(orphanRemoval = true, mappedBy = "parentRole",fetch = FetchType.EAGER)
    private Set<User> users = new HashSet<>();

    @Column(name = "name")
    private String role;
}

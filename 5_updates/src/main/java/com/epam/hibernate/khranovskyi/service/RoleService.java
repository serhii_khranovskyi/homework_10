package com.epam.hibernate.khranovskyi.service;

import com.epam.hibernate.khranovskyi.dto.RoleDto;

import java.util.UUID;

public interface RoleService {
    RoleDto create(RoleDto dto);

    RoleDto get(String roleName);

    RoleDto update(RoleDto dto);

    void delete(UUID id);
}

package com.epam.hibernate;

import com.epam.hibernate.cfg.HibernateConfiguration;
import com.epam.hibernate.khranovskyi.dao.CategoryDAO;
import com.epam.hibernate.khranovskyi.dao.RoleDAO;
import com.epam.hibernate.khranovskyi.dao.UserDAO;
import com.epam.hibernate.khranovskyi.dto.RoleDto;
import com.epam.hibernate.khranovskyi.dto.UserDto;
import com.epam.hibernate.khranovskyi.entity.*;
import com.epam.hibernate.khranovskyi.service.*;
import net.bytebuddy.utility.RandomString;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.NoResultException;
import java.util.HashSet;
import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfiguration.class})
public class UpdateTest {

    @Autowired
    private UserDAO userDao;

    @Autowired
    private RoleDAO roleDAO;

    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserAssembler userAssembler;

    @Autowired
    private CategoryAssembler categoryAssembler;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private UserService userService;

    @Test
    public void saveRole() {
        Role role = new Role();
        role.setRole(RandomString.make(5));
        roleDAO.save(role);
        Assert.assertEquals(roleDAO.get(role.getRole()).getRole(), role.getRole());
    }

    @Test
    public void getRole() {
        Role role = new Role();
        role.setRole(RandomString.make(5));
        roleDAO.save(role);
        Assert.assertEquals(roleDAO.get(role.getRole()).getRole(), role.getRole());
    }

    @Test
    public void updateRoleTest() {
        RoleDto roleDto = new RoleDto();
        roleDto.setRole(RandomString.make(5));
        RoleDto createdRole = roleService.create(roleDto);

        String roleName = RandomString.make(5);
        createdRole.setRole(roleName);

        roleService.update(createdRole);

        Assert.assertEquals(roleDAO.get(roleName).getRole(), roleName);
    }

    @Test
    public void deleteRole() {
        Role role = new Role();
        String roleName = RandomString.make(4);
        role.setRole(roleName);
        roleDAO.save(role);
        roleDAO.delete(roleDAO.get(roleName).getId());
        Assert.assertTrue(verify(roleName, "role"));
    }

    private boolean verify(String name, String switcher) {
        try {
            if (switcher.equals("role"))
                roleDAO.get(name);
            if (switcher.equals("user")) ;
            userDao.get(name);
            if (switcher.equals("category"))
                categoryDAO.get(name);
        } catch (NoResultException e) {
            return true;
        }
        return false;
    }

    @Test
    public void deleteUser() {
        User user = createUser();
        userDao.save(user);
        userDao.delete(user.getId());
        Assert.assertTrue(verify(user.getMail(), "user"));
    }

    @Test
    public void saveUser() {
        User user = createUser();
        userDao.save(user);
        Assert.assertEquals(userDao.get(user.getMail()), user);
    }

    @Test
    public void getUser() {
        User user = createUser();
        userDao.save(user);

        Assert.assertEquals(user.getMail(), userDao.get(user.getMail()).getMail());
    }

    @Test
    public void updateUserTest() {
        String newMail = RandomString.make(5);
        User user = new User();
        user.setFirstName(RandomString.make(5));
        user.setLastName(RandomString.make(5));
        user.setMail(newMail);
        user.setPassword(RandomString.make(5));

        Role role = createRandomRoleFor(user);
        user.setParentRole(role);
        userDao.save(user);

        String first_name = user.getFirstName();
        String last_name = user.getLastName();

        user.setFirstName("Changed_FirstName");
        user.setLastName("Changed_LastName");

        UserDto dto = userService.update(userAssembler.assemble(user));

        Assert.assertNotEquals(first_name, dto.getFirst_name());
        Assert.assertNotEquals(last_name, dto.getLast_name());

    }

    @Test
    public void saveCategory() {
        Category category = createCategory();
        categoryDAO.save(category);

        Assert.assertEquals(categoryDAO.get(category.getCategoryName()).getCategoryName(), category.getCategoryName());
    }

    @Test
    public void getCategory() {
        Category category = createCategory();
        categoryDAO.save(category);

        Assert.assertEquals(categoryDAO.get(category.getCategoryName()).getCategoryName(), category.getCategoryName());
    }

    @Test
    public void deleteCategory() {
        Category category = createCategory();
        categoryDAO.save(category);

        categoryDAO.delete(category.getId());
        Assert.assertTrue(verify(category.getCategoryName(), "category"));
    }

    @Test
    public void updateCategory() {
        Category category = createCategory();
        categoryDAO.save(category);

        category.setCategoryName(RandomString.make(5));
        categoryService.update(categoryAssembler.assemble(category));

        Assert.assertEquals(categoryDAO.get(category.getCategoryName()).getCategoryName(), category.getCategoryName());
    }

    private Category createCategory() {
        Set<Activity> activities = new HashSet<>();
        activities.add(createActivity());

        Set<CategoryLocalization> categoryLocalizations = new HashSet<>();
        categoryLocalizations.add(createCategoryLocalization());

        Category category = new Category();
        category.setCategoryName(RandomString.make(5));
        category.setActivities(activities);
        category.setCategoryLocalizations(categoryLocalizations);

        return category;
    }

    private CategoryLocalization createCategoryLocalization() {
        CategoryLocalization categoryLocalization = new CategoryLocalization();
        categoryLocalization.setTranslation(RandomString.make(5));
        return categoryLocalization;
    }

    private Activity createActivity() {
        Activity activity = new Activity();
        activity.setActivityName(RandomString.make(5));
        return activity;
    }

    private User createUser() {
        User user = new User();
        user.setFirstName(RandomString.make(5));
        user.setLastName(RandomString.make(5));
        user.setMail(RandomString.make(5));
        user.setPassword(RandomString.make(5));

        Role role = createRandomRoleFor(user);
        user.setParentRole(role);
        return user;
    }

    private Role createRandomRoleFor(User user) {
        Set<User> userSet = new HashSet<>();
        userSet.add(user);

        Role role = new Role();

        role.setRole(RandomString.make(5));
        role.setUsers(userSet);

        return role;
    }

}

package com.epam.hibernate.khranovskyi.service;

import com.epam.hibernate.khranovskyi.dto.CategoryDto;
import com.epam.hibernate.khranovskyi.entity.Category;

public interface CategoryAssembler {
    Category assemble(CategoryDto dto);

    CategoryDto assemble(Category entity);
}

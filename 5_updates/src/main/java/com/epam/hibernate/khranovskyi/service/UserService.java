package com.epam.hibernate.khranovskyi.service;

import com.epam.hibernate.khranovskyi.dto.UserDto;

import java.util.UUID;

public interface UserService {
    UserDto create(UserDto dto);

    UserDto get(String userName);

    UserDto update(UserDto dto);

    void delete(UUID id);
}

package com.epam.hibernate.khranovskyi.dao.impl;

import com.epam.hibernate.khranovskyi.dao.LanguageDAO;
import com.epam.hibernate.khranovskyi.entity.Language;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.UUID;

@Repository
@Transactional
public class LanguageDaoImpl implements LanguageDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void save(Language language) {
        Session session = sessionFactory.getCurrentSession();
        session.save(language);
    }

    @Override
    public Language get(UUID languageId) {
        Session session = sessionFactory.getCurrentSession();
        Language language = session.get(Language.class, languageId);
        return Objects.requireNonNull(language, "Role not found by id: " + languageId);
    }

    @Override
    public Language get(String language) {
        return (Language) this.sessionFactory.getCurrentSession().createQuery(
                "from Language l where l.languageName = :name").setParameter("name", language).getSingleResult();
    }

    @Override
    public void delete(UUID languageId) {
        Language language = get(languageId);
        sessionFactory.getCurrentSession().delete(language);
    }
}

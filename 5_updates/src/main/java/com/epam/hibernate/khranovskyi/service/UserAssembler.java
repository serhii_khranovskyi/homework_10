package com.epam.hibernate.khranovskyi.service;

import com.epam.hibernate.khranovskyi.dto.UserDto;
import com.epam.hibernate.khranovskyi.entity.User;

public interface UserAssembler {
    User assemble(UserDto dto);

    UserDto assemble(User entity);
}

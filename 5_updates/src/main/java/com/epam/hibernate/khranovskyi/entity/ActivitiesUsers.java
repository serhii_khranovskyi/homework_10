package com.epam.hibernate.khranovskyi.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "activities_users")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ActivitiesUsers {
    @Id
    @Type(type = "uuid-char")
    private UUID id = UUID.randomUUID();

    private String status;

    private Date beginning;

    private Date ending;

    @ManyToOne
    @JoinColumn(name = "activity_id")
    private Activity parentActivity;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User parentUser;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ActivitiesUsers activitiesUsers = (ActivitiesUsers) o;

        return (parentActivity.equals(activitiesUsers.parentActivity) &
                parentUser.equals(activitiesUsers.parentUser) &
                beginning.toString().equals(beginning.toString()));
    }
}

package com.epam.hibernate.khranovskyi.dao.impl;

import com.epam.hibernate.khranovskyi.dao.CategoryLocalizationDAO;
import com.epam.hibernate.khranovskyi.entity.CategoryLocalization;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.UUID;

@Repository
@Transactional
public class CategoryLocalizationDaoImpl implements CategoryLocalizationDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void save(CategoryLocalization category) {
        Session session = sessionFactory.getCurrentSession();
        session.save(category);
    }

    @Override
    public CategoryLocalization get(UUID categoryLocalizationId) {
        Session session = sessionFactory.getCurrentSession();
        CategoryLocalization category = session.get(CategoryLocalization.class, categoryLocalizationId);
        return Objects.requireNonNull(category, "CategoryLocalization not found by id: " + categoryLocalizationId);
    }

    @Override
    public CategoryLocalization get(String name) {
        return (CategoryLocalization) this.sessionFactory.getCurrentSession().createQuery(
                "from CategoryLocalization c where c.translation = :name").setParameter("name", name).getSingleResult();
    }

    @Override
    public void delete(UUID categoryLocalizationId) {
        CategoryLocalization category = get(categoryLocalizationId);
        sessionFactory.getCurrentSession().delete(category);
    }
}

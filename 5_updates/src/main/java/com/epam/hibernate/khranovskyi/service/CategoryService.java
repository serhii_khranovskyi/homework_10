package com.epam.hibernate.khranovskyi.service;

import com.epam.hibernate.khranovskyi.dto.CategoryDto;

import java.util.UUID;

public interface CategoryService {
    CategoryDto create(CategoryDto dto);

    CategoryDto get(String name);

    CategoryDto update(CategoryDto dto);

    void delete(UUID id);
}

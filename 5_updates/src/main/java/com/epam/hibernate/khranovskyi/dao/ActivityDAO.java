package com.epam.hibernate.khranovskyi.dao;


import com.epam.hibernate.khranovskyi.entity.Activity;

import java.util.UUID;

public interface ActivityDAO {
    void save(Activity activity);

    Activity get(UUID activityId);

    Activity get(String name);

    void delete(UUID activityId);
}
package com.epam.hibernate.khranovskyi.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class ActivitiesUsersDto {
    private UUID id = UUID.randomUUID();

    private String status;

    private Date beginning;

    private Date ending;
}

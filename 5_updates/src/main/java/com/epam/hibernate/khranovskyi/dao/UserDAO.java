package com.epam.hibernate.khranovskyi.dao;

import com.epam.hibernate.khranovskyi.entity.User;

import java.util.UUID;

public interface UserDAO {
    void save(User user);

    User get(UUID userId);

    User get(String mail);

    void delete(UUID userId);
}

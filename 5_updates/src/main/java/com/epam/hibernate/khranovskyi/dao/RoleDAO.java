package com.epam.hibernate.khranovskyi.dao;

import com.epam.hibernate.khranovskyi.entity.Role;

import java.util.UUID;

public interface RoleDAO {
    void save(Role role);

    Role get(UUID roleId);

    Role get(String role);

    void delete(UUID roleId);
}

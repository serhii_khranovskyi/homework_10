package com.epam.hibernate.khranovskyi.dao;

import com.epam.hibernate.khranovskyi.entity.Language;

import java.util.UUID;

public interface LanguageDAO {
    void save(Language language);

    Language get(UUID languageId);

    Language get(String language);

    void delete(UUID languageId);
}
package com.epam.hibernate.khranovskyi.dao.impl;

import com.epam.hibernate.khranovskyi.dao.RoleDAO;
import com.epam.hibernate.khranovskyi.entity.Role;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.UUID;

@Repository
@Transactional
public class RoleDaoImpl implements RoleDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void save(Role role) {
        Session session = sessionFactory.getCurrentSession();
        session.save(role);
    }

    @Override
    public Role get(UUID roleId) {
        Session session = sessionFactory.getCurrentSession();
        Role role = session.get(Role.class, roleId);
        return Objects.requireNonNull(role, "Role not found by id: " + roleId);
    }

    @Override
    public Role get(String role) {
        return (Role) this.sessionFactory.getCurrentSession().createQuery(
                "from Role r where r.role = :roleName").setParameter("roleName", role).getSingleResult();
    }

    @Override
    public void delete(UUID roleId) {
        Role role = get(roleId);
        sessionFactory.getCurrentSession().delete(role);
    }
}

package com.epam.hibernate.khranovskyi.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "categories")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Category {

    @Id
    @Type(type = "uuid-char")
    private UUID id = UUID.randomUUID();

    @Column(name = "category_name")
    private String categoryName;

    @OneToMany(orphanRemoval = true, mappedBy = "parentCategory")
    private Set<Activity> activities = new HashSet<>();

    @OneToMany(orphanRemoval = true, mappedBy = "parentCategoryLocale")
    private Set<CategoryLocalization> categoryLocalizations = new HashSet<>();
}

package com.epam.hibernate.khranovskyi.dao;

import com.epam.hibernate.khranovskyi.entity.CategoryLocalization;

import java.util.UUID;

public interface CategoryLocalizationDAO {
    void save(CategoryLocalization category);

    CategoryLocalization get(UUID categoryLocalizationId);

    CategoryLocalization get(String name);

    void delete(UUID categoryLocalizationId);
}

package com.epam.hibernate.khranovskyi.dao.impl;

import com.epam.hibernate.khranovskyi.dao.ActivityDAO;
import com.epam.hibernate.khranovskyi.entity.Activity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.UUID;

@Repository
@Transactional
public class ActivityDaoImpl implements ActivityDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void delete(UUID activityId) {
        Activity activity = get(activityId);
        sessionFactory.getCurrentSession().delete(activity);
    }

    @Override
    public Activity get(UUID activityId) {
        Session session = sessionFactory.getCurrentSession();
        Activity activity = session.get(Activity.class, activityId);
        return Objects.requireNonNull(activity, "Activity not found by id: " + activityId);
    }

    @Override
    public Activity get(String name) {
        return (Activity) this.sessionFactory.getCurrentSession().createQuery(
                "from Activity a where a.activityName = :name").setParameter("name", name).getSingleResult();

    }

    @Override
    public void save(Activity activity) {
        Session session = sessionFactory.getCurrentSession();
        session.save(activity);
    }
}

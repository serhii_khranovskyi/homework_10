package com.epam.hibernate.khranovskyi.service.impl;

import com.epam.hibernate.khranovskyi.dao.RoleDAO;
import com.epam.hibernate.khranovskyi.dto.RoleDto;
import com.epam.hibernate.khranovskyi.entity.Role;
import com.epam.hibernate.khranovskyi.entity.User;
import com.epam.hibernate.khranovskyi.service.RoleAssembler;
import com.epam.hibernate.khranovskyi.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleDAO roleDAO;

    @Autowired
    private RoleAssembler assembler;

    @Override
    public RoleDto create(RoleDto dto) {
        Role role = assembler.assemble(dto);
        roleDAO.save(role);
        return assembler.assemble(role);
    }

    @Override
    @Transactional(readOnly = true)
    public RoleDto get(String roleName) {
        Role role = roleDAO.get(roleName);
        return assembler.assemble(role);
    }

    @Override
    public void delete(UUID id) {
        roleDAO.delete(id);
    }

    @Override
    @Transactional
    public RoleDto update(RoleDto dto) {
        Role entity = roleDAO.get(dto.getId());
        Role updatedEntity = assembler.assemble(dto);

        performUpdate(entity, updatedEntity);

        return assembler.assemble(entity);
    }

    private void performUpdate(Role persistentEntity, Role newEntity) {
        persistentEntity.setRole(newEntity.getRole());
        updateEntity(persistentEntity.getUsers(), newEntity.getUsers());
    }

    private void updateEntity(Set<User> persistentUsers, Set<User> newUsers){
        Map<UUID, User> stillExistentUsers = newUsers
                .stream()
                .filter(j -> Objects.nonNull(j.getId()))
                .collect(Collectors.toMap(User::getId, Function.identity()));

        Set<User> userSet = newUsers
                .stream()
                .filter(j -> Objects.isNull(j.getId()))
                .collect(Collectors.toSet());

        Iterator<User> persistentIterator = persistentUsers.iterator();
        while (persistentIterator.hasNext()) {
            User persistentUser = persistentIterator.next();
            if (stillExistentUsers
                    .containsKey(persistentUser.getId())) {
                User updatedUser = stillExistentUsers.get(persistentUser.getId());
                persistentUser.setParentRole(updatedUser.getParentRole());
            } else {
                persistentIterator.remove();
                persistentUser.setParentRole(null);
            }
        }
        persistentUsers.addAll(userSet);
    }
}
